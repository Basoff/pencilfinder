import cv2
import numpy as np
from skimage.measure import label, regionprops
import matplotlib.pyplot as plt

#Количество карандашей в данных файлах - 21
#Программа помещается непосредственно в папку с изображениями

cv2.namedWindow("Picture", cv2.WINDOW_KEEPRATIO)
pencils = 0 #Счётчик карандашей

for i in range(1, 13):
    filename = "img (" + str(i) + ").jpg" #Формирует название файла
    img = cv2.imread(filename)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    gray = cv2.GaussianBlur(gray, (155, 155), 0)

    ret,thresh = cv2.threshold(gray,130,255,cv2.THRESH_BINARY_INV)
    lbl = label(thresh/255)

    for reg in regionprops(lbl):
        roundness = reg.area / reg.perimeter**2
        if roundness <= 0.014 and reg.area > 105000: #Ограничение по площади для игнорирования шума, иногда появляющегося по краям картинка
            pencils += 1
            print(reg.bbox)

    cv2.imshow("Picture", img)
    #plt.imshow(lbl)
    #plt.show()


    key = cv2.waitKey(100)
    if key == ord('n'):
        continue
    if key == ord('q'):
        break

print(pencils)
